# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
# ----------------------------------------------------------------------
def ticketInputs():
    # Define global variables for storing number of tickets
    global studentTickets
    global seniorTickets
    global generalTickets
    # Set student/senior/general ticket counts
    studentTickets=input("Enter number of student tickets sold: ")
    seniorTickets=input("Enter number of senior tickets sold: ")
    generalTickets=input("Enter number of general tickets sold: ")
    # Print empty line for formatting
    print()

def inputChecker():
    # Import global ticket values to check if they're valid inputs
    global studentTickets
    global seniorTickets
    global generalTickets
    try:
        # Check if student tickets input is an integer
        studentTickets=int(studentTickets)
    except:
        print("Error: invalid input for number of student tickets sold")
        exit()
    try:
        # Check if senior tickets input is an integer
        seniorTickets=int(seniorTickets)
    except:
        print("Error: invalid input for number of senior tickets")
        exit()
    try:
        # Check if general tickets input is an integer
        generalTickets=int(generalTickets)
    except:
        print("Error: invalid input for number of student tickets")
        exit()
    # Check if any ticket inputs are negative and output error if they are
    if studentTickets < 0 or seniorTickets < 0 or generalTickets < 0:
        print("Error: Tickets can not be less than 0")
        exit()
    

def printTicketsSold():
    # Define global variable for number of tickets sold overall
    global ticketsSold
    # Calculate number of tickets sold overall by adding the number of all types of tickets sold together
    ticketsSold=seniorTickets+studentTickets+generalTickets
    print("Number of tickets sold:",ticketsSold,"\n")

def printTicketRevenue():
    # import global variables for use in revenue calculations
    global studentTickets
    global seniorTickets
    global generalTickets
    global ticketsSold
    # import constants
    STUDENT_PRICE = 9
    SENIOR_PRICE = 12
    GENERAL_PRICE = 15
    # Calculate student revenue and store result in local variable
    studentRevenue=studentTickets*STUDENT_PRICE
    # Calculate senior revenue and store result in local variable
    seniorRevenue=seniorTickets*SENIOR_PRICE
    # Calculate general revenue and store result in local variable
    generalRevenue=generalTickets*GENERAL_PRICE
    # Calculate total revenue and store result in local variable
    totalRevenue=studentRevenue+seniorRevenue+generalRevenue
    # Print results of all calculations with proper formatting
    print("Student tickets generated $",end='')
    print(studentRevenue)
    print("Senior tickets generated $",end='')
    print(seniorRevenue)
    print("General tickets generated $",end='')
    # Add newline in front of total revenue for formatting
    print(generalRevenue,"\n")
    print("Total revenue generated was $",end='')
    print(totalRevenue)
    # Print empty line after total revenue to move the terminal prompt down one line for neatness
    print()
    

# Call all defined functions
ticketInputs()
inputChecker()
printTicketsSold()
printTicketRevenue()