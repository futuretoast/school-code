#!/usr/bin/python3
# Line above lets us call program directly from a posix shell
# ----------------------------------------------------------------------
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
# ----------------------------------------------------------------------
# Import stuff we need except for graphics library
from urllib import request
from pathlib import Path
constPath = Path("./graphics.py")
while constPath.is_file() == False:
    # Download library if not present
    print("Error: Graphics library not found. Attempting to download")
    try:
        request.urlretrieve("https://mcsp.wartburg.edu/zelle/python/graphics.py", "./graphics.py")
    except:
        print("Error! Could not download graphics library. Please check your network settings")
        exit()
# Import functions from graphics library
from graphics import GraphWin, Text, Point, Rectangle, Entry
inputString = ""
mainWin = GraphWin("Stringzy", 640, 480)
print("Start stringzy log")
# Set up header logo into tempVar and draw
tempVar = Text(Point(320,100), "STRINGZY")
tempVar.setSize(36)
tempVar.draw(mainWin)
# Draw more of the intro
Text(Point(320,175), "A Program for doing cool stuff with python strings").draw(mainWin)
# Set up quit botton as quitBox and draw it
quitBox = Rectangle(Point(380, 360), Point(260, 320))
quitBox.setFill('red')
quitBox.draw(mainWin)
Text(Point(320,340), "Quit").draw(mainWin)
enterBox = Rectangle(Point(380, 315), Point(260, 275))
enterBox.setFill('green')
enterBox.draw(mainWin)
inputBox = Entry(Point(320,260), 60)
inputBox.draw(mainWin)
Text(Point(320,295), "Enter").draw(mainWin)
clickPoint = mainWin.getMouse()
print(clickPoint.getX(), clickPoint.getY())
# Run program while the location of the quit button is *not* clicked
while clickPoint.getX() >= 380 or clickPoint.getX() <= 260 or clickPoint.getY() >= 360 or clickPoint.getY() <= 320:
    # Only run the rest of the code if the inside of the enter button is clicked
    if clickPoint.getX() >= 380 or clickPoint.getX() <= 260 or clickPoint.getY() >= 315 or clickPoint.getY() <= 275:
        pass
    else:
        if inputBox.getText() == "":
            errorWin = GraphWin("Error", 400, 50)
            closeBox = Rectangle(Point(350,35), Point(250,10))
            closeBox.setFill('red')
            closeBox.draw(errorWin)
            Text(Point(100,25), "Error! No text entered.").draw(errorWin)
            Text(Point(300,25), "Close").draw(errorWin)
            clickPoint2 = errorWin.getMouse()
            while clickPoint2.getX() >= 350 or clickPoint2.getX() <= 250 or clickPoint2.getY() >= 35 or clickPoint2.getY() <= 10:
                clickPoint2 = errorWin.getMouse()
                print("Windows open")
            errorWin.close()
        else:
            resultWin = GraphWin("Results", 480, 480)
            print("Button clicked")
            Text(Point(240,10), str.title(inputBox.getText())).draw(resultWin)
            Text(Point(240,30), str.upper(inputBox.getText())).draw(resultWin)
            Text(Point(240,50), str.casefold(inputBox.getText())).draw(resultWin)
            print(inputBox.getText().count(" "))
            if inputBox.getText().count(" ") == 0:
                tempVar = "There is 1 word"
                Text(Point(240,70), tempVar).draw(resultWin)
                del tempVar
            else:
                tempVar = "There are "
                tempVar = tempVar + str(inputBox.getText().count(' ')+1)
                tempVar = tempVar + " words."
                Text(Point(240,70), tempVar).draw(resultWin)
                del tempVar
            tempVar = "The letter s shows "  + str(inputBox.getText().count('s')) + ' times.'
            Text(Point(240,90), tempVar).draw(resultWin)
            tempVar = inputBox.getText().split(sep=' ')
            tempVar = str(tempVar[0]) + str(tempVar[len(tempVar) - 1])
            Text(Point(240,110), tempVar).draw(resultWin)
            del tempVar
            tempVar = ""
            for i in inputBox.getText().split(sep=" "):
                tempVar = tempVar + i[0]
            tempVar = tempVar.upper()
            Text(Point(240,130), tempVar).draw(resultWin)
            closeBox = Rectangle(Point(280,420), Point(200,380))
            closeBox.setFill('red')
            closeBox.draw(resultWin)
            Text(Point(240,400), "Close").draw(resultWin)
            clickPoint2 = resultWin.getMouse()
            while clickPoint2.getX() >= 280 or clickPoint2.getX() <= 200 or clickPoint2.getY() >= 420 or clickPoint2.getY() <= 380:
                clickPoint2 = errorWin.getMouse()
                print("Windows open")
            resultWin.close()
        clickPoint = mainWin.getMouse()
        currentInput = inputBox.getText()
        print(currentInput)
print(inputString)