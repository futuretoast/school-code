# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
# ----------------------------------------------------------------------
# Import needed modules
from builtins import input
from sys import exit
def intro():
    print("Welcome to my miles conversion program!")
    print("I will ask for a number of kilometers traveled and then")
    print("display the number of miles you traveled")
    # Declare nameMain and distanceMain as global variables for use in
    # other modules
    global nameMain
    global distanceMain
    # Get inputs to set the variable values
    print("what is your name?",end=" ")
    nameMain = input()
    print("How many kilometers did you travel?",end=" ")
    distanceMain=input()
    return 0

def sanatizeInput():
    # import global distanceMain variable for checking if it's a float/int
    global distanceMain
    distanceTemp = distanceMain
    # Check if distanceMain is an int
    try:
        distanceMain = int(distanceMain)
    except:
        # Check if distanceMain is an float in case it's not an int
        try:
            distanceMain = float(distanceTemp)
        except:
                exit("I wanted a number, not this.")
                return 1
                # Return 1 if distanceMain is neither int or float
        return 0
        # Return 0 if distanceMain is float or int since we can use either
    

def mathMain():
    # Declare global variable distanceActual so we can print it in
    # the output module
    global distanceActual
    # Import variable distanceMain for calculation
    global distanceMain
    # convert distanceMain to kilometers and store the result int distanceActual
    distanceActual = distanceMain*0.621371
    # Return 0 on success
    return 0

def outro():
    # Import variables we want to display to user
    global nameMain
    global distanceMain
    global distanceActual
    # Congragulate user
    print("Great job ",nameMain)
    # Give user desired result
    print(distanceMain,"kilometers equals ",distanceActual,"miles!")
    print("Thank you for using my program, keep up the good work!")
    # Return 0 on success
    return 0

# Run all modules
intro()
sanatizeInput()
mathMain()
outro()