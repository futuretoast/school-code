#!/usr/bin/python3.5
# Add shebang in first line for calling script directly from a posix shell
#===============================================================================
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
#===============================================================================
# Ask user for grade and set variable in one line
userGrade = input("Please enter your average grade: ")
try:
    # Check if user input is an integer
    userGrade = int(userGrade)
except:
    try:
        # If user input is not an integer chck if it is a float
        userGrade = float(userGrade)
    except:
        # If user input is neither float nor int assume input is invalid
        print("Error: Invalid input for grade!")
        exit()
# Check if grade entered is less than 0
if userGrade < 0:
    print("Error: Grade can not be less than 0!")
    exit()
# Check if grade entered is over 100
elif userGrade > 100:
    print("Error: Grade can not be over 100!")
    exit()
else:
    # If grade is nether over 100 nor under 0 proceed to ask user for days missed and set variable
    userDaysMissed = input("Please enter number of days missed: ")
    try:
        # Check if days missed it a whole numbber
        userDaysMissed = int(userDaysMissed)
    except:
        print("Error: Invalid input for days missed! ")
        exit()
    # Check if user grade is over 96 and succeed if it is
    if userGrade >= 96:
        print("You're exempt from the final for having a 96% or above in the class")
        exit()
    # Check is user grade is over 93 (We can safely assume it's under 96 since the
    # last part failed) and days missed are less than 3 and tell user they are exempt if true
    elif userGrade >= 93 and userDaysMissed < 3:
        print("You're exempt from the final for having a 93% or above and less than 3 days missed")
        exit()
    # Check if user grade is over 90 (we can safely assumt it's under 96 and 93 since the previus
    # tries failed) and there are no days missed. Tell user they are exempt if true
    elif userGrade >= 90 and userDaysMissed == 0:
        print("You're exempt from the final for having over a 90% and perfect atendance")
        exit()
    else:
        # Tell user they're not exempt fromt the final if no conditions are true
        print("You're not exempt from the final")
    