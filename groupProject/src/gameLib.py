# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
# ----------------------------------------------------------------------
# TODO: Update comments to reflect separation of game code and libs

# Print game intro and get user's name
def writeToLog(dataToWrite,logFileName="log.log"):
    logFileName = open(logFileName, "a")
    dataToWrite = str(dataToWrite) + '\n'
    logFileName.write(dataToWrite)
    logFileName.close()
def gameIntro():
    from time import sleep
    print("Welcome to \"What's the card?\" A simple card game.")
    userName = input("Care to tell me your name?: ")
    sleep(1)
    clearGameScreen()
    return(userName)

# Code for main game menu lib
def gameMenu():
    # Display main menu
    menuChoice = input("1.) See rules\n2.) Play game\n3.) See another example\n4.) Exit\n")
    # Check if user entered a value choice and convert to integer if true
    if menuChoice == "1" or menuChoice == "2" or menuChoice == "3" or menuChoice == "4":
        menuChoice = int(menuChoice)
        # Return menu choice for use in other functions
        return(menuChoice)
    else:
        # Return false if invalid input is detected
        return(False)
    

def printRules():
    from time import sleep
    # Print actual rules
    print("The rules are simple.")
    print("I'll show 5 cards and then")
    print("ask what they are one at a ")
    print("time . Since this is played")
    print("on a keyboard you use \"s\"")
    print(" for spades,\"h\" for hearts")
    print("\"c\", for clubs, and \"d\"")
    print("for diamonds since you can't")
    print("type \"♠\", \"♥\", \"♣\" or")
    print("\"♦\". If you select correctly")
    print("You get 10 points, but if you're")
    print("incorrect you lose 20")
    # Wait 5 seconds to clear screen so user can actually read the rules
    sleep(1)
    input("Press enter to continue")
    clearGameScreen()

def printRulesExit():
    from time import sleep
    # Print actual rules
    print("Shame you're not playing")
    print("The rules were simple.")
    print("I'd show 5 cards and then")
    print("ask what they were. You'd just")
    print("type the answer to get points")
    print("I hope you come back to play")
    # Wait 3 seconds for user to read exit message
    sleep(3)
    # Clear screen and exit
    clearGameScreen()
    exit()

def genCard():
    from random import randint
    from time import sleep
    # Define suits in a list
    suitList = ["s", "h", "c", "d"]
    # Set card number to a random number between 1 and 13
    numberValue = randint(1,13)
    # Pick a random suit from the list of suits
    suitValue = suitList[randint(0,3)]
    # Convert numberValue to a string so we can
    # concatenate suitValue to it
    numberValue = str(numberValue)
    # Declare cardValue as global
    cardValue = numberValue + suitValue
    return(cardValue)


    # Clear screen, generate card,
    # then show card on screen
    clearGameScreen()
    genCard()
    print(cardValue)
    # Wait 1 second before clearing screen
    # to give the user a chance to win
    sleep(1)
    clearGameScreen()
    
def clearGameScreen():
    from sys import platform
    from os import system
    # Check if on windows
    if platform == "win32":
        # Set clear screen command to cls
        # on windows
        clearScreen = "cls"
    elif platform == "linux" or "darwin" or "cygwin":
        # If not on windows check if we are
        # on a mac/linux/cygwin
        clearScreen = "clear"
    else:
        # Display error on platforms where we are not sure if screen can be cleared.
        # Default to "clear" command and hope it works
        print("Platform unsupported. Significant errors may occur.")
        clearScreen = "clear"
    system(clearScreen)
# Actually tracks number of wins/loses despite the name
def currentScore(didUserWin, winCount = 0):
    winCount = int(winCount)
    if didUserWin == True:
        winCount = winCount + 1
        return(winCount)
    elif didUserWin == False:
        winCount = winCount - 2
        return(winCount)
    elif didUserWin == "Final":
        winCount = winCount * 10
        return(winCount)
    else:
        # Print error if function does not execute correctly
        print("Unknown error")
        print("Technical info: Unknown error in currentScore function")
    winCount = winCount * 10
    return(winCount)
# Lib for asking if user wants to play again
def replayChoice():
    from time import sleep
    clearGameScreen()
    # Get user's choice with friendly prompt
    print("Do you want to play again?")
    userChoice = input("[y/n] ")
    # Check if y was entered
    if userChoice == "y":
        # Clear screen and restart without demo
        clearGameScreen()
        return(True)
    elif userChoice == "n":
        # Say goodbye to user if they answer n
        print("I hope to see you again!")
        # Wait so user can read the goodbye message
        sleep(3)
        # Clear game screen for clean exit
        clearGameScreen()
        return(False)
    else:
        # Return a None value if user enters invalid choice
        return(None)
    
def gameDemo():
    from random import randint
    # Randomly choose to win or loose
    demoChoice = randint(1,2)
    if demoChoice == 1:
        demoWin()
    elif demoChoice == 2:
        demoLose()
    demoReplayChoice()

# Function for a point gain example
def demoWin():
    from time import sleep
    print("This is an example game-play")
    print("This will show you what to expect")
    sleep(2)
    clearGameScreen()
    # Set card value
    cardValue = genCard()
    # Print card value, wait and clear screen
    print(cardValue)
    sleep(1)
    clearGameScreen()
    # Print card enter prompt without newline
    print("Please enter the card shown: ",end='')
    # Wait then print card value
    sleep(1)
    print(cardValue)
    sleep(1)
    # Print score is 10
    # TODO: Implement counter for demo score
    print("Your score is 10")
    sleep(2)
    clearGameScreen()
def demoLose():
    from time import sleep
    print("This is an example game-play")
    print("This will show you what to expect")
    # Set card value
    cardValue = genCard()
    # Print card value, wait and clear screen
    print(cardValue)
    sleep(1)
    clearGameScreen()
    # Print card prompt without newline
    print("Please enter the card shown: ",end='')
    # Generate a card for the simulated user to enter
    tempCardValue = genCard()
    if tempCardValue != cardValue:
        # Directly print card value if not equal
        print(tempCardValue)
    elif tempCardValue == cardValue and tempCardValue == "2h":
        # Check if static value and print value if not
        print("4d")
    else:
        # If generated card equals static value
        # print different static value
        print("2h")
    sleep(2)
    # Print that simulated user lost 20 points.
    print("Your score is -20")
    sleep(1)
    clearGameScreen()
def demoReplayChoice():
    from time import sleep
    # Simulate user choosing to replay game
    print("Want to play again?")
    print("[y/n] ",end='')
    sleep(1)
    print("y")
    sleep(1)
    clearGameScreen()