#!/usr/bin/python3
# Line above lets us call program directly from a linux shell
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
# ----------------------------------------------------------------------
# Check if library files exist and stop program if not
from pathlib import Path
constPath = Path("./gameLib.py")
if constPath.is_file():
    # Do nothing if library is found
    pass
else:
    print("Error: Game libs not found. Please place gameLib.py in the same folder.")
    exit()
# Import functions from gameLib
from time import sleep
from gameLib import *
# Import demo functions
# Show 3 example and then move to game menu
for temp in range(5):
    gameDemo()
# Deinitialize temp variable since we don't need it's contents anymore
del temp
sleep(1)
clearGameScreen()
clearGameScreen()
gameIntro()
# Display the actual menu
menuChoice = gameMenu()
while menuChoice != 4:
    # If user chooses option 1 print rules and reopen menu
    if menuChoice == 1:
        clearGameScreen()
        printRules()
        menuChoice = gameMenu()
    # If user chooses option 2 print rules and exit game
    elif menuChoice == 2:
        clearGameScreen()
        printRules()
        # Do "ready, set, go" countdown to game
        print("Ready?..")
        sleep(1)
        clearGameScreen()
        print("Set...")
        sleep(1)
        clearGameScreen()
        print("GO!")
        sleep(1)
        currentScoreValue = 0
        # This is litterally all that changed.
        # Added 3 new card scenarios by increasing the loop counter from 7 to 10
        # since they're dynamically generated
        for runCount in range(13):
            # Run main game 5 times, clearing the screen before each run
            clearGameScreen()
            # Set card value
            cardValue = genCard()
            print(cardValue)
            sleep(1)
            clearGameScreen()
            # Get user input for what they think the card is
            userCard = input("Please enter the card shown: ")
            # Check if card user entered is the actual
            # card value and ask them to play again if it is
            if cardValue == userCard:
                # Print "Correct!" and display current score
                currentScoreValue = currentScore(True, currentScoreValue)
                print("Correct! Your current score is",currentScoreValue*10)
                sleep(1)
                logValue = 'runCount' + str(runCount) + '/' + str(currentScoreValue)
                writeToLog(logValue, "cards.txt")
            else:
                #  Print "Incorrect!" and display current score
                currentScoreValue = currentScore(False, currentScoreValue)
                print("Incorrect! Your current score is",currentScoreValue*10)
                sleep(1)
                logValue = 'runCount' + str(runCount) + '/' + str(currentScoreValue)
                writeToLog(logValue, "cards.txt")
            clearGameScreen()
        # Print final score
        print("Your final score is", currentScore("Final",currentScoreValue))
        # Wait 3 seconds and then ask user if they want to replay the game
        sleep(3)
        clearGameScreen()
        replayChoiceValue = replayChoice()
        while replayChoiceValue != False:
            if replayChoiceValue == True:
                break
            else:
                print("Invalid option! Use \"y\" or \"n\"")
                sleep(3)
                clearGameScreen()
                replayChoiceValue = replayChoice()
        
        if replayChoiceValue == False:
            print("I hope to see you again!")
            sleep(2)
            clearGameScreen()
            exit()
        else:
            menuChoice = gameMenu()
    # If user chooses option 3 dynamically create a card scenario
    elif menuChoice == 3:
        clearGameScreen()
        gameDemo()
        menuChoice = gameMenu()
    # Probably gonna silently depreciate this bit of the game
    # If user chooses option 4 print modified rules text and exit
    # elif menuChoice == 4:
    #    clearGameScreen()
    #     printRulesExit()
    # If user entered an invalid option print error and reopen menu
    elif menuChoice == False:
        # Clear screen and restart game menu if input is invalid
        print("That's not an option!")
        sleep(1)
        clearGameScreen()
