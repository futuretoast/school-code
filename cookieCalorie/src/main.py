# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
# ---------------------------------------------------------------------
# Define prereq module (for importing needed prerequisites)
from pathlib import Path
# TODO: Allow user to change contants.py location.
# Check if constants.py is in the same folder as main.py and set global variable to pass result to main
def prereq():
    # Declare actual variable
    global whereIsConstPath
    # Define expected path for contants.py/check it
    constpath = Path("./constants.py")
    if constpath.is_file():
        # Import constant if default path is correct
        whereIsConstPath = 1
    else:
        # Exit with status code 1 if contants.py is not present
        print("ERR: constants.py not in same folder as main.py! Exiting")
        return 1
        exit()
    

# Define main module
def main():
    # Get result of constants.py location checking/setting
    global whereIsConstPath
    # Shim code for constants.py path customization
    if whereIsConstPath == 1:
        from constants import COOKIE_ENERGY
        # Ask for user input
        cookieCount = input("Enter number of cookies consumed by the relevant entity: ")
        # Attempt to reject strings and negative numbers since people shouldn't be eating
        # negative cookies
        try:
            # Check if cookies eaten is a whole number
            cookieCount = int(cookieCount)
        except:
            try:
                cookieCount = float(cookieCount)
            except:
                print("You know that's not a number right?")
                # return 2 if input is string
                return 2
                exit()
            
        
        consumedEnergy = cookieCount*COOKIE_ENERGY
        if consumedEnergy < 0:
            print("I'm not sure how you eat negative cookies")
            # return 3 if value is negative
            return 3
            exit()
        print("You have consumed ",consumedEnergy," units of energy")
    else:
        print("ERR: Something went really wrong since this error shouldn't ever be displayed")
        exit()
    

prereq()
main()